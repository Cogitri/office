# Copyright 2009-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.12 1.11 ] ]

SUMMARY="Library for converting WMF files"
DESCRIPTION="
libwmf is a library for reading vector images in Microsoft's native Windows
Metafile Format (WMF) and for converting them to more standard/open file formats
such as SVG, EPS, PS, FIG, PNG and JPEG.
"
HOMEPAGE="http://wvware.sourceforge.net/libwmf.html"
DOWNLOADS="mirror://sourceforge/wvware/${PNV}.tar.gz"

#BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-text/ghostscript[>=9.22&<9.23] [[ note = [ hard-coded path via src_configure ] ]]
        dev-libs/expat[>=2.0.1]
        media-libs/freetype:2[>=2.0.1]
        media-libs/libpng:=
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-intoverflow.patch
    "${FILES}"/${PNV}-build.patch
    "${FILES}"/${PNV}-pngfix.patch
    "${FILES}"/${PNV}-remove_checks.patch
    "${FILES}"/${PNV}-png15.patch
    "${FILES}"/${PNV}-CAN-2004-0941.patch
    "${FILES}"/${PNV}-CVE-2007-0455.patch
    "${FILES}"/${PNV}-CVE-2007-2756.patch
    "${FILES}"/${PNV}-CVE-2007-3472.patch
    "${FILES}"/${PNV}-CVE-2007-3473.patch
    "${FILES}"/${PNV}-CVE-2007-3477.patch
    "${FILES}"/${PNV}-CVE-2009-3546.patch
    "${FILES}"/${PNV}-CVE-2015-0848_CVE-2015-4588.patch
    "${FILES}"/${PNV}-CVE-2015-4695.patch
    "${FILES}"/${PNV}-CVE-2015-4696.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-gd
    --disable-static
    --with-docdir=/usr/share/doc/${PNVR}
    --with-expat
    --with-fontdir=/usr/share/libwmf/fonts
    --with-gsfontdir=/usr/share/fonts/default/ghostscript
    --with-gsfontmap=/usr/share/ghostscript/9.22/Resource/Init/Fontmap.GS
    --without-x
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )

